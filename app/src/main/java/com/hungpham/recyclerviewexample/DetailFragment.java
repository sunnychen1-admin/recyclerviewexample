package com.hungpham.recyclerviewexample;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hungpham.recyclerviewexample.databinding.FragmentDetailBinding;
import com.hungpham.recyclerviewexample.databinding.FragmentMainBinding;

public class DetailFragment extends Fragment {
    private FragmentDetailBinding binding;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentDetailBinding.inflate(getLayoutInflater());
        return binding.getRoot();
    }
}