package com.hungpham.recyclerviewexample;

public class Item {
    public String name;
    public int backgroundResource;
    public Item(String name, int backgroundResource) {
        this.name = name;
        this.backgroundResource = backgroundResource;
    }
}
