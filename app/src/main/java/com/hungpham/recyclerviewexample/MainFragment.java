package com.hungpham.recyclerviewexample;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.hungpham.recyclerviewexample.databinding.FragmentMainBinding;

import java.util.ArrayList;

public class MainFragment extends Fragment implements ScenicAdapter.ClickListener {
    private FragmentMainBinding binding;
    private ScenicAdapter adapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentMainBinding.inflate(getLayoutInflater());
        adapter = new ScenicAdapter(fetchData(),this);
        binding.scenicRecyclerView.setAdapter(adapter);
        binding.scenicRecyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
        return binding.getRoot();
    }

    private ArrayList<Item> fetchData() {
        ArrayList<Item> items = new ArrayList<>();
        items.add(new Item("Item 1", R.drawable.bg1));
        items.add(new Item("Item 2", R.drawable.bg2));
        items.add(new Item("Item 3", R.drawable.bg3));
        items.add(new Item("Item 4", R.drawable.bg4));
        items.add(new Item("Item 5", R.drawable.bg5));
        items.add(new Item("Item 6", R.drawable.bg6));
        items.add(new Item("Item 7", R.drawable.bg7));
        return items;
    }

    @Override
    public void onItemClick(int position) {
        Item item = adapter.items.get(position);
        Toast.makeText(this.getContext(), "Item: " + item.name, Toast.LENGTH_SHORT).show();
//        NavHostFragment.findNavController(this).navigate(R.id.action_mainFragment_to_detailFragment,bundle);
    }
}